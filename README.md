##### Prerequisites
`docker` 20.10+, `docker-compose` 1.29+

if wanna try `SSL Offloading`, try [gen-self-signed-cert.sh](gen-self-signed-cert.sh):
```bash
./gen-self-signed-cert.sh <your-domain>
```
You can just skip it because I had prepared cert stuff for `127.0.0.1` inside [cert/](cert/).

##### Run it
As always:
```bash
docker-compose up -d && docker-compose logs -f
```

##### Debugging with net tool
```bash
docker run -it --net haproxy_fishnet nicolaka/netshoot
```

##### Reload HAProxy config
Update your config file, then validate it:
```bash
docker-compose exec haproxy haproxy -c -f /etc/haproxy/test.cfg
```
Gracefully reload:
```bash
docker kill -s HUP haproxy_haproxy_1
```
